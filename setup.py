#!/usr/bin/env python

from setuptools import setup, find_packages


def get_version_and_cmdclass(package_path):
    """Load version.py module without importing the whole package.

    Template code from miniver
    """
    import os
    from importlib.util import module_from_spec, spec_from_file_location
    spec = spec_from_file_location('version',
                                   os.path.join(package_path, '_version.py'))
    module = module_from_spec(spec)
    spec.loader.exec_module(module)
    return module.__version__, module.cmdclass


version, cmdclass = get_version_and_cmdclass('qs_dclab')

setup(
    name='quantumsim-dclab',
    url='https://quantumsim.gitlab.io/',
    version=version,
    cmdclass=cmdclass,
    description=(
        'A model for simulating superconducting transmon qubits in '
        'Quantumsim, DiCarlo Lab (TU Delft)-specific.'
    ),
    author='Quantumsim Authors',
    author_email='V.Ostroukh@tudelft.nl',
    packages=find_packages('.'),
    ext_package='qs_dclab',
    install_requires=['quantumsim>0', 'numpy', 'scipy'],
)
