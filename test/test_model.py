import os
import pytest
from qs_dclab import DiCarloLabModel
from quantumsim import Setup
from quantumsim.circuits import Gate

test_dir = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture(scope='session')
def model():
    setup = Setup.from_file(os.path.join(test_dir, '..', 'setup_perfect.yaml'))
    return DiCarloLabModel(setup)


class TestModel:
    def test_cphase(self, model):
        # CPhase can be created
        assert isinstance(model.cphase('Q0', 'Q1'), Gate)

    def test_rotate(self, model):
        assert isinstance(model.rotate('Q'), Gate)
        assert isinstance(model.rotate_x('Q'), Gate)
        assert isinstance(model.rotate_y('Q'), Gate)

    def test_measure(self, model):
        assert isinstance(model.measure_partial('Q'), Gate)
        assert isinstance(model.measure_project('Q'), Gate)
